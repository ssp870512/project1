# EduGate docs

## EduGate

EduGate is a system for managing student applications to high schools. It contains Web app where users can send their applications, Desktop app for administration and shared database for both projects.

this project tests and deploys docs on gitlab pages generated from markdown files.

### Tech Stack

* ASP.NET Core
* WPF
* Custom ORM for interaction with database using .NET reflection

## project requirements

* [x] GIT repozitář na Gitlabu.
  * [x] Repozitář má README.md (K čemu projekt slouží, jak se používá, jak se instaluje, jaké jsou prerekvizity, apod.)
* [x] Použití libovolného generátoru statických stránek dle vlastního výběru. (MKdocs, Middleman, Jekyll, apod.)
* [x] Vytvořená CI v repozitáři.
* [x] CI má minimálně dvě úlohy:
  * [x] Test kvality Markdown stránek.
  * [x] Generování HTML stránek z Markdown zdrojů.
* [x] CI má automatickou úlohou nasazení web stránek (deploy).
* [x] Gitlab projekt má fukční web stránky s generovaným obsahem na URL:
